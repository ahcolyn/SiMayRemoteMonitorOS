﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMay.RemoteControls.Core.Enum
{
    public enum BrandColorMode
    {
        X1 = 1,
        X4 = 4,
        X16 = 16
    }
}
